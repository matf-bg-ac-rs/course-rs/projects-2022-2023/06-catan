/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer_6;
    QSpacerItem *verticalSpacer_5;
    QPushButton *pbExit;
    QSpacerItem *verticalSpacer;
    QSpacerItem *verticalSpacer_4;
    QPushButton *pbHelp;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pbStartGame;
    QPushButton *pbSettings;
    QWidget *page_2;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QWidget *widget;
    QLabel *label;
    QPushButton *pbBackToMenu;
    QGroupBox *gbox1;
    QRadioButton *rbON;
    QRadioButton *rbOFF;
    QGroupBox *gbox2;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *horizontalSpacer_4;
    QWidget *page_3;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_3;
    QPushButton *pbBackToMenu_2;
    QSpacerItem *horizontalSpacer_9;
    QSpacerItem *horizontalSpacer_5;
    QWidget *widget_2;
    QWidget *page_4;
    QWidget *gridLayoutWidget_4;
    QGridLayout *gridLayout_4;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *verticalSpacer_7;
    QSpacerItem *verticalSpacer_12;
    QSpacerItem *verticalSpacer_10;
    QSpacerItem *verticalSpacer_13;
    QSpacerItem *verticalSpacer_11;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lbPlayer3;
    QLineEdit *lePlayer3;
    QSpacerItem *verticalSpacer_9;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lbPlayer2;
    QLineEdit *lePlayer2;
    QPushButton *pbBackToMenu_3;
    QSpacerItem *verticalSpacer_8;
    QHBoxLayout *horizontalLayout_6;
    QLabel *lbPlayer4;
    QLineEdit *lePlayer4;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lbPlayer1;
    QLineEdit *lePlayer1;
    QPushButton *pbContinue;
    QLabel *lbPlayer2Required;
    QLabel *lbPlayer3Required;
    QLabel *lbPlayer4Required;
    QLabel *lbPlayer1Required;
    QWidget *page_5;
    QWidget *widget_4;
    QGroupBox *groupBox;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_8;
    QLabel *lbPlayer1Name;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label_30;
    QLabel *lbPointsPlayer1;
    QLabel *label_9;
    QLabel *lbRoadsPlayer1;
    QWidget *horizontalLayoutWidget_6;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_10;
    QLabel *lbWheatP1;
    QLabel *label_6;
    QLabel *lbWoolP1;
    QLabel *label_8;
    QLabel *lbStoneP1;
    QLabel *label_7;
    QLabel *lbWoodP1;
    QLabel *label_3;
    QLabel *lbBrickP1;
    QGroupBox *groupBox_2;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_9;
    QLabel *lbPlayer2Name;
    QSpacerItem *horizontalSpacer_10;
    QLabel *label_31;
    QLabel *lbPointsPlayer2;
    QLabel *label_12;
    QLabel *lbRoadsPlayer2;
    QWidget *horizontalLayoutWidget_7;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_11;
    QLabel *lbWheatP2;
    QLabel *label_13;
    QLabel *lbWoolP2;
    QLabel *label_15;
    QLabel *lbStoneP2;
    QLabel *label_17;
    QLabel *lbWoodP2;
    QLabel *label_18;
    QLabel *lbBrickP2;
    QGroupBox *groupBox_3;
    QWidget *horizontalLayoutWidget_4;
    QHBoxLayout *horizontalLayout_10;
    QLabel *lbPlayer3Name;
    QSpacerItem *horizontalSpacer_11;
    QLabel *label_33;
    QLabel *lbPointsPlayer3;
    QLabel *label_14;
    QLabel *lbRoadsPlayer3;
    QWidget *horizontalLayoutWidget_8;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_19;
    QLabel *lbWheatP3;
    QLabel *label_20;
    QLabel *lbWoolP3;
    QLabel *label_21;
    QLabel *lbStoneP3;
    QLabel *label_22;
    QLabel *lbWoodP3;
    QLabel *label_23;
    QLabel *lbBrickP3;
    QGroupBox *groupBox_4;
    QWidget *horizontalLayoutWidget_5;
    QHBoxLayout *horizontalLayout_11;
    QLabel *lbPlayer4Name;
    QSpacerItem *horizontalSpacer_12;
    QLabel *label_35;
    QLabel *lbPointsPlayer4;
    QLabel *label_16;
    QLabel *lbRoadsPlayer4;
    QWidget *horizontalLayoutWidget_9;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_24;
    QLabel *lbWheatP4;
    QLabel *label_25;
    QLabel *lbWoolP4;
    QLabel *label_26;
    QLabel *lbStoneP4;
    QLabel *label_27;
    QLabel *lbWoodP4;
    QLabel *label_28;
    QLabel *lbBrickP4;
    QGroupBox *groupBox_5;
    QPushButton *pushButton;
    QLabel *label_4;
    QLabel *lbPlayerTurn;
    QWidget *horizontalLayoutWidget_11;
    QHBoxLayout *horizontalLayout_7;
    QComboBox *cbTradePlayer;
    QLabel *label_5;
    QComboBox *cbTradeBank;
    QPushButton *pbTrade;
    QWidget *widget_5;
    QWidget *widget_8;
    QPushButton *pb_Road;
    QPushButton *pb_Settlement;
    QPushButton *pb_MagicCard;
    QPushButton *pb_House;
    QWidget *widget_9;
    QWidget *horizontalLayoutWidget_10;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_29;
    QLabel *lbWheatBank;
    QLabel *label_32;
    QLabel *lbWoolBank;
    QLabel *label_34;
    QLabel *lbStoneBank;
    QLabel *label_36;
    QLabel *lbWoodBank;
    QLabel *label_37;
    QLabel *lbBrickBank;
    QLabel *lbBank;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QWidget *wDice1;
    QWidget *wDice2;
    QGraphicsView *gvMapa;
    QPushButton *pbRollDice;
    QLabel *label_2;
    QLabel *label_38;
    QLabel *label_39;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_40;
    QLabel *label_44;
    QLabel *label_41;
    QLabel *label_45;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_43;
    QLabel *label_46;
    QLabel *label_42;
    QLabel *label_47;
    QWidget *horizontalLayoutWidget_14;
    QHBoxLayout *horizontalLayout_25;
    QLabel *label_52;
    QLabel *label_53;
    QLabel *label_54;
    QLabel *label_55;
    QWidget *horizontalLayoutWidget_15;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_56;
    QLabel *label_57;
    QLabel *label_58;
    QLabel *label_59;
    QLabel *label_48;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_49;
    QLabel *label_50;
    QLabel *label_51;
    QLabel *label_60;
    QWidget *layoutWidget_2;
    QHBoxLayout *horizontalLayout_27;
    QLabel *label_61;
    QLabel *label_62;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1200, 800);
        MainWindow->setMinimumSize(QSize(1200, 800));
        MainWindow->setMaximumSize(QSize(1200, 800));
        MainWindow->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/pozadina.jpg) 0 0 0 0 stretch stretch;"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        centralwidget->setStyleSheet(QString::fromUtf8("border-image: url(:/images/menuBackground.jpg) 0 0 0 0 stretch stretch;\n"
""));
        stackedWidget = new QStackedWidget(centralwidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(10, 10, 1191, 771));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        gridLayoutWidget = new QWidget(page);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(50, 20, 1081, 711));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_6, 8, 2, 1, 1);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_5, 6, 2, 1, 1);

        pbExit = new QPushButton(gridLayoutWidget);
        pbExit->setObjectName(QString::fromUtf8("pbExit"));
        pbExit->setMinimumSize(QSize(400, 80));
        pbExit->setMaximumSize(QSize(400, 80));
        QFont font;
        font.setFamily(QString::fromUtf8("Sahadeva"));
        font.setPointSize(37);
        font.setBold(true);
        font.setWeight(75);
        pbExit->setFont(font);
        pbExit->setStyleSheet(QString::fromUtf8("background-color: #c28a93;\n"
"color:#fff3ab;\n"
"border: 1px solid #fff3ab;\n"
"border-color: #fff3ab;"));

        gridLayout->addWidget(pbExit, 7, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 2, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_4, 11, 2, 1, 1);

        pbHelp = new QPushButton(gridLayoutWidget);
        pbHelp->setObjectName(QString::fromUtf8("pbHelp"));
        pbHelp->setMinimumSize(QSize(60, 50));
        pbHelp->setMaximumSize(QSize(60, 50));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sahadeva"));
        font1.setPointSize(30);
        font1.setBold(true);
        font1.setWeight(75);
        pbHelp->setFont(font1);
        pbHelp->setStyleSheet(QString::fromUtf8("background-color:#89c3d7;\n"
"width: 60;\n"
"height: 50;\n"
""));

        gridLayout->addWidget(pbHelp, 0, 3, 1, 1, Qt::AlignRight);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 1, 3, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 9, 2, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 4, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);

        pbStartGame = new QPushButton(gridLayoutWidget);
        pbStartGame->setObjectName(QString::fromUtf8("pbStartGame"));
        pbStartGame->setMinimumSize(QSize(400, 80));
        pbStartGame->setMaximumSize(QSize(400, 80));
        pbStartGame->setFont(font);
        pbStartGame->setStyleSheet(QString::fromUtf8("background-color: #c28a93;\n"
"color:#fff3ab;\n"
"border: 1px solid #fff3ab;\n"
"border-color: #fff3ab;"));

        gridLayout->addWidget(pbStartGame, 3, 2, 1, 1);

        pbSettings = new QPushButton(gridLayoutWidget);
        pbSettings->setObjectName(QString::fromUtf8("pbSettings"));
        pbSettings->setMinimumSize(QSize(400, 80));
        pbSettings->setMaximumSize(QSize(400, 80));
        pbSettings->setFont(font);
        pbSettings->setStyleSheet(QString::fromUtf8("background-color: #c28a93;\n"
"color:#fff3ab;\n"
"border: 1px solid #fff3ab;\n"
"border-color: #fff3ab;"));

        gridLayout->addWidget(pbSettings, 5, 2, 1, 1);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        gridLayoutWidget_2 = new QWidget(page_2);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(10, 19, 1161, 802));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        widget = new QWidget(gridLayoutWidget_2);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMinimumSize(QSize(600, 600));
        widget->setMaximumSize(QSize(600, 600));
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(110, 60, 400, 80));
        label->setMinimumSize(QSize(400, 80));
        label->setMaximumSize(QSize(400, 80));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sahadeva"));
        font2.setPointSize(28);
        font2.setBold(true);
        font2.setWeight(75);
        label->setFont(font2);
        label->setStyleSheet(QString::fromUtf8("background-color: #c28a93;\n"
"color:#fff3ab;\n"
"border: 1px solid #fff3ab;\n"
"border-color: #fff3ab;"));
        label->setAlignment(Qt::AlignCenter);
        pbBackToMenu = new QPushButton(widget);
        pbBackToMenu->setObjectName(QString::fromUtf8("pbBackToMenu"));
        pbBackToMenu->setGeometry(QRect(110, 360, 400, 80));
        pbBackToMenu->setMinimumSize(QSize(400, 80));
        pbBackToMenu->setMaximumSize(QSize(400, 80));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Sahadeva"));
        font3.setPointSize(22);
        font3.setBold(true);
        font3.setWeight(75);
        pbBackToMenu->setFont(font3);
        pbBackToMenu->setStyleSheet(QString::fromUtf8("background-color: #c28a93;\n"
"color:#fff3ab;\n"
"border: 1px solid #fff3ab;\n"
"border-color: #fff3ab;"));
        gbox1 = new QGroupBox(widget);
        gbox1->setObjectName(QString::fromUtf8("gbox1"));
        gbox1->setGeometry(QRect(410, 50, 120, 91));
        gbox1->setStyleSheet(QString::fromUtf8("border:none;"));
        rbON = new QRadioButton(gbox1);
        rbON->setObjectName(QString::fromUtf8("rbON"));
        rbON->setGeometry(QRect(30, 20, 112, 23));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Sahadeva"));
        font4.setPointSize(15);
        font4.setBold(true);
        font4.setWeight(75);
        rbON->setFont(font4);
        rbON->setStyleSheet(QString::fromUtf8("color:#fff3ab;\n"
""));
        rbOFF = new QRadioButton(gbox1);
        rbOFF->setObjectName(QString::fromUtf8("rbOFF"));
        rbOFF->setGeometry(QRect(30, 50, 112, 23));
        rbOFF->setFont(font4);
        rbOFF->setStyleSheet(QString::fromUtf8("color:#fff3ab;"));
        gbox2 = new QGroupBox(widget);
        gbox2->setObjectName(QString::fromUtf8("gbox2"));
        gbox2->setGeometry(QRect(430, 170, 120, 80));
        gbox2->setStyleSheet(QString::fromUtf8("border: none;"));

        gridLayout_2->addWidget(widget, 0, 1, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 0, 0, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 0, 2, 1, 1);

        stackedWidget->addWidget(page_2);
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        page_3->setStyleSheet(QString::fromUtf8(""));
        gridLayoutWidget_3 = new QWidget(page_3);
        gridLayoutWidget_3->setObjectName(QString::fromUtf8("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(10, 20, 1161, 802));
        gridLayout_3 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        pbBackToMenu_2 = new QPushButton(gridLayoutWidget_3);
        pbBackToMenu_2->setObjectName(QString::fromUtf8("pbBackToMenu_2"));
        pbBackToMenu_2->setMaximumSize(QSize(200, 80));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Sahadeva"));
        font5.setPointSize(12);
        font5.setBold(true);
        font5.setWeight(75);
        pbBackToMenu_2->setFont(font5);
        pbBackToMenu_2->setStyleSheet(QString::fromUtf8("background-color: #c28a93;\n"
"color:#fff3ab;\n"
"border: 1px solid #fff3ab;\n"
"border-color: #fff3ab;"));

        gridLayout_3->addWidget(pbBackToMenu_2, 1, 0, 1, 1);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_9, 0, 3, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_5, 0, 0, 1, 1);

        widget_2 = new QWidget(gridLayoutWidget_3);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setMinimumSize(QSize(600, 400));
        widget_2->setMaximumSize(QSize(600, 550));
        widget_2->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/rlsss.png) 0 0 0 0 stretch stretch;"));

        gridLayout_3->addWidget(widget_2, 0, 2, 1, 1);

        stackedWidget->addWidget(page_3);
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        gridLayoutWidget_4 = new QWidget(page_4);
        gridLayoutWidget_4->setObjectName(QString::fromUtf8("gridLayoutWidget_4"));
        gridLayoutWidget_4->setGeometry(QRect(0, 0, 1181, 771));
        gridLayout_4 = new QGridLayout(gridLayoutWidget_4);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_6, 0, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));

        gridLayout_4->addLayout(horizontalLayout_5, 11, 1, 1, 1);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_7, 0, 3, 1, 1);

        verticalSpacer_12 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_12, 8, 3, 1, 1);

        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_10, 4, 3, 1, 1);

        verticalSpacer_13 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_13, 9, 3, 1, 1);

        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_11, 6, 3, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lbPlayer3 = new QLabel(gridLayoutWidget_4);
        lbPlayer3->setObjectName(QString::fromUtf8("lbPlayer3"));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Sahadeva"));
        font6.setPointSize(30);
        lbPlayer3->setFont(font6);
        lbPlayer3->setStyleSheet(QString::fromUtf8("background-color: \"green\";\n"
"color:#fff3ab;\n"
"border-radius: 10%;"));

        horizontalLayout_4->addWidget(lbPlayer3);

        lePlayer3 = new QLineEdit(gridLayoutWidget_4);
        lePlayer3->setObjectName(QString::fromUtf8("lePlayer3"));
        QFont font7;
        font7.setFamily(QString::fromUtf8("Sahadeva"));
        font7.setPointSize(20);
        lePlayer3->setFont(font7);
        lePlayer3->setStyleSheet(QString::fromUtf8("border: 6px solid \"green\";\n"
"border-radius: 10%;"));

        horizontalLayout_4->addWidget(lePlayer3);


        gridLayout_4->addLayout(horizontalLayout_4, 5, 3, 1, 1);

        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_9, 2, 3, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        lbPlayer2 = new QLabel(gridLayoutWidget_4);
        lbPlayer2->setObjectName(QString::fromUtf8("lbPlayer2"));
        lbPlayer2->setFont(font6);
        lbPlayer2->setStyleSheet(QString::fromUtf8("background-color: #d9bf16;\n"
"color:#fff3ab;\n"
"border-radius: 10%;"));

        horizontalLayout_3->addWidget(lbPlayer2);

        lePlayer2 = new QLineEdit(gridLayoutWidget_4);
        lePlayer2->setObjectName(QString::fromUtf8("lePlayer2"));
        lePlayer2->setFont(font7);
        lePlayer2->setStyleSheet(QString::fromUtf8("border: 6px solid #d9bf16;\n"
"border-radius: 10%;"));

        horizontalLayout_3->addWidget(lePlayer2);


        gridLayout_4->addLayout(horizontalLayout_3, 3, 3, 1, 1);

        pbBackToMenu_3 = new QPushButton(gridLayoutWidget_4);
        pbBackToMenu_3->setObjectName(QString::fromUtf8("pbBackToMenu_3"));
        pbBackToMenu_3->setMinimumSize(QSize(300, 80));
        pbBackToMenu_3->setMaximumSize(QSize(300, 80));
        pbBackToMenu_3->setFont(font);
        pbBackToMenu_3->setStyleSheet(QString::fromUtf8("background-color: #c28a93;\n"
"color:#fff3ab;\n"
"border: 1px solid #fff3ab;\n"
"border-color: #fff3ab;\n"
"border-radius: 10%;"));

        gridLayout_4->addWidget(pbBackToMenu_3, 9, 0, 1, 1);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_8, 11, 3, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        lbPlayer4 = new QLabel(gridLayoutWidget_4);
        lbPlayer4->setObjectName(QString::fromUtf8("lbPlayer4"));
        lbPlayer4->setFont(font6);
        lbPlayer4->setStyleSheet(QString::fromUtf8("background-color: \"red\";\n"
"color:#fff3ab;\n"
"border-radius: 10%;"));

        horizontalLayout_6->addWidget(lbPlayer4);

        lePlayer4 = new QLineEdit(gridLayoutWidget_4);
        lePlayer4->setObjectName(QString::fromUtf8("lePlayer4"));
        lePlayer4->setFont(font7);
        lePlayer4->setStyleSheet(QString::fromUtf8("border: 6px solid \"red\";\n"
"border-radius: 10%;"));

        horizontalLayout_6->addWidget(lePlayer4);


        gridLayout_4->addLayout(horizontalLayout_6, 7, 3, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_7, 0, 4, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lbPlayer1 = new QLabel(gridLayoutWidget_4);
        lbPlayer1->setObjectName(QString::fromUtf8("lbPlayer1"));
        lbPlayer1->setFont(font6);
        lbPlayer1->setStyleSheet(QString::fromUtf8("background-color: #1f45cf;\n"
"color:#fff3ab;\n"
"border-radius: 10%;"));

        horizontalLayout_2->addWidget(lbPlayer1);

        lePlayer1 = new QLineEdit(gridLayoutWidget_4);
        lePlayer1->setObjectName(QString::fromUtf8("lePlayer1"));
        lePlayer1->setFont(font7);
        lePlayer1->setStyleSheet(QString::fromUtf8("border: 6px solid #1f45cf;\n"
"border-radius: 10%;\n"
""));
        lePlayer1->setFrame(true);

        horizontalLayout_2->addWidget(lePlayer1);


        gridLayout_4->addLayout(horizontalLayout_2, 1, 3, 1, 1);

        pbContinue = new QPushButton(gridLayoutWidget_4);
        pbContinue->setObjectName(QString::fromUtf8("pbContinue"));
        pbContinue->setMinimumSize(QSize(300, 80));
        pbContinue->setMaximumSize(QSize(300, 80));
        pbContinue->setFont(font);
        pbContinue->setLayoutDirection(Qt::RightToLeft);
        pbContinue->setStyleSheet(QString::fromUtf8("background-color: #c28a93;\n"
"color:#fff3ab;\n"
"border: 1px solid #fff3ab;\n"
"border-color: #fff3ab;\n"
"border-radius: 10%;"));

        gridLayout_4->addWidget(pbContinue, 9, 4, 1, 1);

        lbPlayer2Required = new QLabel(gridLayoutWidget_4);
        lbPlayer2Required->setObjectName(QString::fromUtf8("lbPlayer2Required"));
        QFont font8;
        font8.setFamily(QString::fromUtf8("Sahadeva"));
        font8.setPointSize(13);
        lbPlayer2Required->setFont(font8);
        lbPlayer2Required->setStyleSheet(QString::fromUtf8("color : \"white\";"));

        gridLayout_4->addWidget(lbPlayer2Required, 3, 4, 1, 1);

        lbPlayer3Required = new QLabel(gridLayoutWidget_4);
        lbPlayer3Required->setObjectName(QString::fromUtf8("lbPlayer3Required"));
        lbPlayer3Required->setFont(font8);
        lbPlayer3Required->setStyleSheet(QString::fromUtf8("color : \"white\";"));

        gridLayout_4->addWidget(lbPlayer3Required, 5, 4, 1, 1);

        lbPlayer4Required = new QLabel(gridLayoutWidget_4);
        lbPlayer4Required->setObjectName(QString::fromUtf8("lbPlayer4Required"));
        lbPlayer4Required->setFont(font8);
        lbPlayer4Required->setStyleSheet(QString::fromUtf8("color : \"white\";"));

        gridLayout_4->addWidget(lbPlayer4Required, 7, 4, 1, 1);

        lbPlayer1Required = new QLabel(gridLayoutWidget_4);
        lbPlayer1Required->setObjectName(QString::fromUtf8("lbPlayer1Required"));
        lbPlayer1Required->setFont(font8);
        lbPlayer1Required->setStyleSheet(QString::fromUtf8("color : \"white\";"));

        gridLayout_4->addWidget(lbPlayer1Required, 1, 4, 1, 1);

        stackedWidget->addWidget(page_4);
        page_5 = new QWidget();
        page_5->setObjectName(QString::fromUtf8("page_5"));
        widget_4 = new QWidget(page_5);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        widget_4->setGeometry(QRect(820, 0, 350, 550));
        widget_4->setMinimumSize(QSize(350, 550));
        widget_4->setMaximumSize(QSize(350, 16777215));
        widget_4->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/boardR.png) 0 0 0 0 stretch stretch;"));
        groupBox = new QGroupBox(widget_4);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 20, 331, 80));
        groupBox->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/wood.png) 0 0 0 0 stretch stretch;"));
        horizontalLayoutWidget_2 = new QWidget(groupBox);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 0, 311, 35));
        horizontalLayout_8 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        lbPlayer1Name = new QLabel(horizontalLayoutWidget_2);
        lbPlayer1Name->setObjectName(QString::fromUtf8("lbPlayer1Name"));
        QFont font9;
        font9.setPointSize(18);
        font9.setBold(true);
        font9.setWeight(75);
        lbPlayer1Name->setFont(font9);
        lbPlayer1Name->setStyleSheet(QString::fromUtf8("color: #1f45cf;"));

        horizontalLayout_8->addWidget(lbPlayer1Name);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_8);

        label_30 = new QLabel(horizontalLayoutWidget_2);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        QFont font10;
        font10.setBold(true);
        font10.setWeight(75);
        label_30->setFont(font10);

        horizontalLayout_8->addWidget(label_30);

        lbPointsPlayer1 = new QLabel(horizontalLayoutWidget_2);
        lbPointsPlayer1->setObjectName(QString::fromUtf8("lbPointsPlayer1"));
        QFont font11;
        font11.setPointSize(18);
        lbPointsPlayer1->setFont(font11);

        horizontalLayout_8->addWidget(lbPointsPlayer1);

        label_9 = new QLabel(horizontalLayoutWidget_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_8->addWidget(label_9);

        lbRoadsPlayer1 = new QLabel(horizontalLayoutWidget_2);
        lbRoadsPlayer1->setObjectName(QString::fromUtf8("lbRoadsPlayer1"));
        lbRoadsPlayer1->setFont(font11);

        horizontalLayout_8->addWidget(lbRoadsPlayer1);

        horizontalLayoutWidget_6 = new QWidget(groupBox);
        horizontalLayoutWidget_6->setObjectName(QString::fromUtf8("horizontalLayoutWidget_6"));
        horizontalLayoutWidget_6->setGeometry(QRect(0, 40, 321, 41));
        horizontalLayout_12 = new QHBoxLayout(horizontalLayoutWidget_6);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(0, 0, 0, 0);
        label_10 = new QLabel(horizontalLayoutWidget_6);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--grain.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_12->addWidget(label_10);

        lbWheatP1 = new QLabel(horizontalLayoutWidget_6);
        lbWheatP1->setObjectName(QString::fromUtf8("lbWheatP1"));
        lbWheatP1->setFont(font11);

        horizontalLayout_12->addWidget(lbWheatP1);

        label_6 = new QLabel(horizontalLayoutWidget_6);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--wool.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_12->addWidget(label_6);

        lbWoolP1 = new QLabel(horizontalLayoutWidget_6);
        lbWoolP1->setObjectName(QString::fromUtf8("lbWoolP1"));
        lbWoolP1->setFont(font11);

        horizontalLayout_12->addWidget(lbWoolP1);

        label_8 = new QLabel(horizontalLayoutWidget_6);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--ore.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_12->addWidget(label_8);

        lbStoneP1 = new QLabel(horizontalLayoutWidget_6);
        lbStoneP1->setObjectName(QString::fromUtf8("lbStoneP1"));
        lbStoneP1->setFont(font11);

        horizontalLayout_12->addWidget(lbStoneP1);

        label_7 = new QLabel(horizontalLayoutWidget_6);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--lumber.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_12->addWidget(label_7);

        lbWoodP1 = new QLabel(horizontalLayoutWidget_6);
        lbWoodP1->setObjectName(QString::fromUtf8("lbWoodP1"));
        lbWoodP1->setFont(font11);

        horizontalLayout_12->addWidget(lbWoodP1);

        label_3 = new QLabel(horizontalLayoutWidget_6);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--brick.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_12->addWidget(label_3);

        lbBrickP1 = new QLabel(horizontalLayoutWidget_6);
        lbBrickP1->setObjectName(QString::fromUtf8("lbBrickP1"));
        lbBrickP1->setFont(font11);

        horizontalLayout_12->addWidget(lbBrickP1);

        groupBox_2 = new QGroupBox(widget_4);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 110, 331, 80));
        groupBox_2->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/wood.png) 0 0 0 0 stretch stretch;"));
        horizontalLayoutWidget_3 = new QWidget(groupBox_2);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(10, 0, 311, 35));
        horizontalLayout_9 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        lbPlayer2Name = new QLabel(horizontalLayoutWidget_3);
        lbPlayer2Name->setObjectName(QString::fromUtf8("lbPlayer2Name"));
        lbPlayer2Name->setFont(font9);
        lbPlayer2Name->setStyleSheet(QString::fromUtf8("color: #d9bf16;\n"
""));

        horizontalLayout_9->addWidget(lbPlayer2Name);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_10);

        label_31 = new QLabel(horizontalLayoutWidget_3);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setFont(font10);

        horizontalLayout_9->addWidget(label_31);

        lbPointsPlayer2 = new QLabel(horizontalLayoutWidget_3);
        lbPointsPlayer2->setObjectName(QString::fromUtf8("lbPointsPlayer2"));
        lbPointsPlayer2->setFont(font11);

        horizontalLayout_9->addWidget(lbPointsPlayer2);

        label_12 = new QLabel(horizontalLayoutWidget_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout_9->addWidget(label_12);

        lbRoadsPlayer2 = new QLabel(horizontalLayoutWidget_3);
        lbRoadsPlayer2->setObjectName(QString::fromUtf8("lbRoadsPlayer2"));
        lbRoadsPlayer2->setFont(font11);

        horizontalLayout_9->addWidget(lbRoadsPlayer2);

        horizontalLayoutWidget_7 = new QWidget(groupBox_2);
        horizontalLayoutWidget_7->setObjectName(QString::fromUtf8("horizontalLayoutWidget_7"));
        horizontalLayoutWidget_7->setGeometry(QRect(0, 40, 321, 41));
        horizontalLayout_13 = new QHBoxLayout(horizontalLayoutWidget_7);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        horizontalLayout_13->setContentsMargins(0, 0, 0, 0);
        label_11 = new QLabel(horizontalLayoutWidget_7);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--grain.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_13->addWidget(label_11);

        lbWheatP2 = new QLabel(horizontalLayoutWidget_7);
        lbWheatP2->setObjectName(QString::fromUtf8("lbWheatP2"));
        lbWheatP2->setFont(font11);

        horizontalLayout_13->addWidget(lbWheatP2);

        label_13 = new QLabel(horizontalLayoutWidget_7);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--wool.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_13->addWidget(label_13);

        lbWoolP2 = new QLabel(horizontalLayoutWidget_7);
        lbWoolP2->setObjectName(QString::fromUtf8("lbWoolP2"));
        lbWoolP2->setFont(font11);

        horizontalLayout_13->addWidget(lbWoolP2);

        label_15 = new QLabel(horizontalLayoutWidget_7);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--ore.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_13->addWidget(label_15);

        lbStoneP2 = new QLabel(horizontalLayoutWidget_7);
        lbStoneP2->setObjectName(QString::fromUtf8("lbStoneP2"));
        lbStoneP2->setFont(font11);

        horizontalLayout_13->addWidget(lbStoneP2);

        label_17 = new QLabel(horizontalLayoutWidget_7);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--lumber.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_13->addWidget(label_17);

        lbWoodP2 = new QLabel(horizontalLayoutWidget_7);
        lbWoodP2->setObjectName(QString::fromUtf8("lbWoodP2"));
        lbWoodP2->setFont(font11);

        horizontalLayout_13->addWidget(lbWoodP2);

        label_18 = new QLabel(horizontalLayoutWidget_7);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--brick.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_13->addWidget(label_18);

        lbBrickP2 = new QLabel(horizontalLayoutWidget_7);
        lbBrickP2->setObjectName(QString::fromUtf8("lbBrickP2"));
        lbBrickP2->setFont(font11);

        horizontalLayout_13->addWidget(lbBrickP2);

        groupBox_3 = new QGroupBox(widget_4);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 200, 331, 80));
        groupBox_3->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/wood.png) 0 0 0 0 stretch stretch;"));
        horizontalLayoutWidget_4 = new QWidget(groupBox_3);
        horizontalLayoutWidget_4->setObjectName(QString::fromUtf8("horizontalLayoutWidget_4"));
        horizontalLayoutWidget_4->setGeometry(QRect(10, 0, 311, 35));
        horizontalLayout_10 = new QHBoxLayout(horizontalLayoutWidget_4);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        lbPlayer3Name = new QLabel(horizontalLayoutWidget_4);
        lbPlayer3Name->setObjectName(QString::fromUtf8("lbPlayer3Name"));
        lbPlayer3Name->setFont(font9);
        lbPlayer3Name->setStyleSheet(QString::fromUtf8("color: green;\n"
""));

        horizontalLayout_10->addWidget(lbPlayer3Name);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_11);

        label_33 = new QLabel(horizontalLayoutWidget_4);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setFont(font10);

        horizontalLayout_10->addWidget(label_33);

        lbPointsPlayer3 = new QLabel(horizontalLayoutWidget_4);
        lbPointsPlayer3->setObjectName(QString::fromUtf8("lbPointsPlayer3"));
        lbPointsPlayer3->setFont(font11);

        horizontalLayout_10->addWidget(lbPointsPlayer3);

        label_14 = new QLabel(horizontalLayoutWidget_4);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_10->addWidget(label_14);

        lbRoadsPlayer3 = new QLabel(horizontalLayoutWidget_4);
        lbRoadsPlayer3->setObjectName(QString::fromUtf8("lbRoadsPlayer3"));
        lbRoadsPlayer3->setFont(font11);

        horizontalLayout_10->addWidget(lbRoadsPlayer3);

        horizontalLayoutWidget_8 = new QWidget(groupBox_3);
        horizontalLayoutWidget_8->setObjectName(QString::fromUtf8("horizontalLayoutWidget_8"));
        horizontalLayoutWidget_8->setGeometry(QRect(0, 40, 321, 41));
        horizontalLayout_14 = new QHBoxLayout(horizontalLayoutWidget_8);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        horizontalLayout_14->setContentsMargins(0, 0, 0, 0);
        label_19 = new QLabel(horizontalLayoutWidget_8);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--grain.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_14->addWidget(label_19);

        lbWheatP3 = new QLabel(horizontalLayoutWidget_8);
        lbWheatP3->setObjectName(QString::fromUtf8("lbWheatP3"));
        lbWheatP3->setFont(font11);

        horizontalLayout_14->addWidget(lbWheatP3);

        label_20 = new QLabel(horizontalLayoutWidget_8);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--wool.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_14->addWidget(label_20);

        lbWoolP3 = new QLabel(horizontalLayoutWidget_8);
        lbWoolP3->setObjectName(QString::fromUtf8("lbWoolP3"));
        lbWoolP3->setFont(font11);

        horizontalLayout_14->addWidget(lbWoolP3);

        label_21 = new QLabel(horizontalLayoutWidget_8);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--ore.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_14->addWidget(label_21);

        lbStoneP3 = new QLabel(horizontalLayoutWidget_8);
        lbStoneP3->setObjectName(QString::fromUtf8("lbStoneP3"));
        lbStoneP3->setFont(font11);

        horizontalLayout_14->addWidget(lbStoneP3);

        label_22 = new QLabel(horizontalLayoutWidget_8);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--lumber.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_14->addWidget(label_22);

        lbWoodP3 = new QLabel(horizontalLayoutWidget_8);
        lbWoodP3->setObjectName(QString::fromUtf8("lbWoodP3"));
        lbWoodP3->setFont(font11);

        horizontalLayout_14->addWidget(lbWoodP3);

        label_23 = new QLabel(horizontalLayoutWidget_8);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--brick.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_14->addWidget(label_23);

        lbBrickP3 = new QLabel(horizontalLayoutWidget_8);
        lbBrickP3->setObjectName(QString::fromUtf8("lbBrickP3"));
        lbBrickP3->setFont(font11);

        horizontalLayout_14->addWidget(lbBrickP3);

        groupBox_4 = new QGroupBox(widget_4);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(10, 290, 331, 80));
        groupBox_4->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/wood.png) 0 0 0 0 stretch stretch;"));
        horizontalLayoutWidget_5 = new QWidget(groupBox_4);
        horizontalLayoutWidget_5->setObjectName(QString::fromUtf8("horizontalLayoutWidget_5"));
        horizontalLayoutWidget_5->setGeometry(QRect(10, 0, 311, 35));
        horizontalLayout_11 = new QHBoxLayout(horizontalLayoutWidget_5);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        lbPlayer4Name = new QLabel(horizontalLayoutWidget_5);
        lbPlayer4Name->setObjectName(QString::fromUtf8("lbPlayer4Name"));
        lbPlayer4Name->setFont(font9);
        lbPlayer4Name->setStyleSheet(QString::fromUtf8("color: red;\n"
""));

        horizontalLayout_11->addWidget(lbPlayer4Name);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_12);

        label_35 = new QLabel(horizontalLayoutWidget_5);
        label_35->setObjectName(QString::fromUtf8("label_35"));
        label_35->setFont(font10);

        horizontalLayout_11->addWidget(label_35);

        lbPointsPlayer4 = new QLabel(horizontalLayoutWidget_5);
        lbPointsPlayer4->setObjectName(QString::fromUtf8("lbPointsPlayer4"));
        lbPointsPlayer4->setFont(font11);

        horizontalLayout_11->addWidget(lbPointsPlayer4);

        label_16 = new QLabel(horizontalLayoutWidget_5);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        horizontalLayout_11->addWidget(label_16);

        lbRoadsPlayer4 = new QLabel(horizontalLayoutWidget_5);
        lbRoadsPlayer4->setObjectName(QString::fromUtf8("lbRoadsPlayer4"));
        lbRoadsPlayer4->setFont(font11);

        horizontalLayout_11->addWidget(lbRoadsPlayer4);

        horizontalLayoutWidget_9 = new QWidget(groupBox_4);
        horizontalLayoutWidget_9->setObjectName(QString::fromUtf8("horizontalLayoutWidget_9"));
        horizontalLayoutWidget_9->setGeometry(QRect(0, 40, 321, 41));
        horizontalLayout_15 = new QHBoxLayout(horizontalLayoutWidget_9);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        horizontalLayout_15->setContentsMargins(0, 0, 0, 0);
        label_24 = new QLabel(horizontalLayoutWidget_9);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--grain.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_15->addWidget(label_24);

        lbWheatP4 = new QLabel(horizontalLayoutWidget_9);
        lbWheatP4->setObjectName(QString::fromUtf8("lbWheatP4"));
        lbWheatP4->setFont(font11);

        horizontalLayout_15->addWidget(lbWheatP4);

        label_25 = new QLabel(horizontalLayoutWidget_9);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--wool.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_15->addWidget(label_25);

        lbWoolP4 = new QLabel(horizontalLayoutWidget_9);
        lbWoolP4->setObjectName(QString::fromUtf8("lbWoolP4"));
        lbWoolP4->setFont(font11);

        horizontalLayout_15->addWidget(lbWoolP4);

        label_26 = new QLabel(horizontalLayoutWidget_9);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--ore.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_15->addWidget(label_26);

        lbStoneP4 = new QLabel(horizontalLayoutWidget_9);
        lbStoneP4->setObjectName(QString::fromUtf8("lbStoneP4"));
        lbStoneP4->setFont(font11);

        horizontalLayout_15->addWidget(lbStoneP4);

        label_27 = new QLabel(horizontalLayoutWidget_9);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--lumber.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_15->addWidget(label_27);

        lbWoodP4 = new QLabel(horizontalLayoutWidget_9);
        lbWoodP4->setObjectName(QString::fromUtf8("lbWoodP4"));
        lbWoodP4->setFont(font11);

        horizontalLayout_15->addWidget(lbWoodP4);

        label_28 = new QLabel(horizontalLayoutWidget_9);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--brick.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_15->addWidget(label_28);

        lbBrickP4 = new QLabel(horizontalLayoutWidget_9);
        lbBrickP4->setObjectName(QString::fromUtf8("lbBrickP4"));
        lbBrickP4->setFont(font11);

        horizontalLayout_15->addWidget(lbBrickP4);

        groupBox_5 = new QGroupBox(widget_4);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(10, 460, 331, 80));
        groupBox_5->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/wood.png) 0 0 0 0 stretch stretch;"));
        pushButton = new QPushButton(groupBox_5);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(190, 0, 141, 81));
        pushButton->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/next.png) 0 0 0 0 stretch stretch;"));
        label_4 = new QLabel(groupBox_5);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(0, 0, 181, 41));
        label_4->setFont(font4);
        label_4->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/transparent.png) 0 0 0 0 stretch stretch;"));
        label_4->setAlignment(Qt::AlignCenter);
        lbPlayerTurn = new QLabel(groupBox_5);
        lbPlayerTurn->setObjectName(QString::fromUtf8("lbPlayerTurn"));
        lbPlayerTurn->setGeometry(QRect(0, 40, 181, 41));
        QFont font12;
        font12.setFamily(QString::fromUtf8("Sahadeva"));
        font12.setPointSize(18);
        font12.setBold(true);
        font12.setItalic(true);
        font12.setUnderline(false);
        font12.setWeight(75);
        lbPlayerTurn->setFont(font12);
        lbPlayerTurn->setStyleSheet(QString::fromUtf8(""));
        lbPlayerTurn->setAlignment(Qt::AlignCenter);
        horizontalLayoutWidget_11 = new QWidget(widget_4);
        horizontalLayoutWidget_11->setObjectName(QString::fromUtf8("horizontalLayoutWidget_11"));
        horizontalLayoutWidget_11->setGeometry(QRect(10, 370, 331, 41));
        horizontalLayout_7 = new QHBoxLayout(horizontalLayoutWidget_11);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        cbTradePlayer = new QComboBox(horizontalLayoutWidget_11);
        cbTradePlayer->setObjectName(QString::fromUtf8("cbTradePlayer"));
        QFont font13;
        font13.setPointSize(13);
        cbTradePlayer->setFont(font13);
        cbTradePlayer->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/wood.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_7->addWidget(cbTradePlayer);

        label_5 = new QLabel(horizontalLayoutWidget_11);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QFont font14;
        font14.setFamily(QString::fromUtf8("Sahadeva"));
        font14.setPointSize(16);
        font14.setBold(true);
        font14.setWeight(75);
        label_5->setFont(font14);
        label_5->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_5);

        cbTradeBank = new QComboBox(horizontalLayoutWidget_11);
        cbTradeBank->setObjectName(QString::fromUtf8("cbTradeBank"));
        cbTradeBank->setFont(font13);
        cbTradeBank->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/wood.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_7->addWidget(cbTradeBank);

        pbTrade = new QPushButton(widget_4);
        pbTrade->setObjectName(QString::fromUtf8("pbTrade"));
        pbTrade->setGeometry(QRect(120, 410, 111, 41));
        pbTrade->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/trade_button.png) 0 0 0 0 stretch stretch;\n"
"border-radius: 10%;\n"
"\n"
""));
        widget_5 = new QWidget(page_5);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        widget_5->setGeometry(QRect(0, 659, 801, 111));
        widget_5->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/boardR.png) 0 0 0 0 stretch stretch;"));
        widget_8 = new QWidget(widget_5);
        widget_8->setObjectName(QString::fromUtf8("widget_8"));
        widget_8->setGeometry(QRect(370, 20, 410, 81));
        widget_8->setMinimumSize(QSize(410, 0));
        widget_8->setMaximumSize(QSize(340, 16777215));
        widget_8->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/wood.png) 0 0 0 0 stretch stretch;"));
        pb_Road = new QPushButton(widget_8);
        pb_Road->setObjectName(QString::fromUtf8("pb_Road"));
        pb_Road->setGeometry(QRect(150, 10, 71, 61));
        pb_Road->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/button_road.png) 0 0 0 0 stretch stretch;"));
        pb_Settlement = new QPushButton(widget_8);
        pb_Settlement->setObjectName(QString::fromUtf8("pb_Settlement"));
        pb_Settlement->setGeometry(QRect(310, 10, 71, 61));
        pb_Settlement->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/button_settlement.png) 0 0 0 0 stretch stretch;"));
        pb_MagicCard = new QPushButton(widget_8);
        pb_MagicCard->setObjectName(QString::fromUtf8("pb_MagicCard"));
        pb_MagicCard->setGeometry(QRect(30, 10, 71, 61));
        pb_MagicCard->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/button_magicCard.png) 0 0 0 0 stretch stretch;"));
        pb_House = new QPushButton(widget_8);
        pb_House->setObjectName(QString::fromUtf8("pb_House"));
        pb_House->setGeometry(QRect(230, 10, 71, 61));
        pb_House->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/button_house.png) 0 0 0 0 stretch stretch;"));
        widget_9 = new QWidget(widget_5);
        widget_9->setObjectName(QString::fromUtf8("widget_9"));
        widget_9->setGeometry(QRect(10, 20, 340, 81));
        widget_9->setMinimumSize(QSize(340, 0));
        widget_9->setMaximumSize(QSize(340, 16777215));
        widget_9->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/wood.png) 0 0 0 0 stretch stretch;"));
        horizontalLayoutWidget_10 = new QWidget(widget_9);
        horizontalLayoutWidget_10->setObjectName(QString::fromUtf8("horizontalLayoutWidget_10"));
        horizontalLayoutWidget_10->setGeometry(QRect(10, 40, 321, 41));
        horizontalLayout_16 = new QHBoxLayout(horizontalLayoutWidget_10);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        horizontalLayout_16->setContentsMargins(0, 0, 0, 0);
        label_29 = new QLabel(horizontalLayoutWidget_10);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--grain.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_16->addWidget(label_29);

        lbWheatBank = new QLabel(horizontalLayoutWidget_10);
        lbWheatBank->setObjectName(QString::fromUtf8("lbWheatBank"));
        lbWheatBank->setFont(font11);

        horizontalLayout_16->addWidget(lbWheatBank);

        label_32 = new QLabel(horizontalLayoutWidget_10);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--wool.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_16->addWidget(label_32);

        lbWoolBank = new QLabel(horizontalLayoutWidget_10);
        lbWoolBank->setObjectName(QString::fromUtf8("lbWoolBank"));
        lbWoolBank->setFont(font11);

        horizontalLayout_16->addWidget(lbWoolBank);

        label_34 = new QLabel(horizontalLayoutWidget_10);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--ore.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_16->addWidget(label_34);

        lbStoneBank = new QLabel(horizontalLayoutWidget_10);
        lbStoneBank->setObjectName(QString::fromUtf8("lbStoneBank"));
        lbStoneBank->setFont(font11);

        horizontalLayout_16->addWidget(lbStoneBank);

        label_36 = new QLabel(horizontalLayoutWidget_10);
        label_36->setObjectName(QString::fromUtf8("label_36"));
        label_36->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--lumber.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_16->addWidget(label_36);

        lbWoodBank = new QLabel(horizontalLayoutWidget_10);
        lbWoodBank->setObjectName(QString::fromUtf8("lbWoodBank"));
        lbWoodBank->setFont(font11);

        horizontalLayout_16->addWidget(lbWoodBank);

        label_37 = new QLabel(horizontalLayoutWidget_10);
        label_37->setObjectName(QString::fromUtf8("label_37"));
        label_37->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--brick.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_16->addWidget(label_37);

        lbBrickBank = new QLabel(horizontalLayoutWidget_10);
        lbBrickBank->setObjectName(QString::fromUtf8("lbBrickBank"));
        lbBrickBank->setFont(font11);

        horizontalLayout_16->addWidget(lbBrickBank);

        lbBank = new QLabel(widget_9);
        lbBank->setObjectName(QString::fromUtf8("lbBank"));
        lbBank->setGeometry(QRect(10, 0, 93, 29));
        lbBank->setFont(font9);
        lbBank->setStyleSheet(QString::fromUtf8("color: green;\n"
"\n"
""));
        horizontalLayoutWidget = new QWidget(page_5);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(820, 560, 351, 161));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        wDice1 = new QWidget(horizontalLayoutWidget);
        wDice1->setObjectName(QString::fromUtf8("wDice1"));
        wDice1->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/dice6.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout->addWidget(wDice1);

        wDice2 = new QWidget(horizontalLayoutWidget);
        wDice2->setObjectName(QString::fromUtf8("wDice2"));
        wDice2->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/dice6.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout->addWidget(wDice2);

        gvMapa = new QGraphicsView(page_5);
        gvMapa->setObjectName(QString::fromUtf8("gvMapa"));
        gvMapa->setGeometry(QRect(0, 0, 800, 650));
        gvMapa->setMinimumSize(QSize(800, 650));
        gvMapa->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/catanMap.png) 0 0 0 0 stretch stretch;"));
        pbRollDice = new QPushButton(page_5);
        pbRollDice->setObjectName(QString::fromUtf8("pbRollDice"));
        pbRollDice->setGeometry(QRect(820, 724, 351, 51));
        pbRollDice->setStyleSheet(QString::fromUtf8("background-color: #c28a93;\n"
"color:#fff3ab;\n"
"border: 1px solid #fff3ab;\n"
"border-color: #fff3ab;\n"
"border-radius: 10%;"));
        label_2 = new QLabel(page_5);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(598, 0, 51, 46));
        label_2->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/button_house.png) 0 0 0 0 stretch stretch;"));
        label_38 = new QLabel(page_5);
        label_38->setObjectName(QString::fromUtf8("label_38"));
        label_38->setGeometry(QRect(0, 0, 51, 46));
        label_38->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/button_road.png) 0 0 0 0 stretch stretch;"));
        label_39 = new QLabel(page_5);
        label_39->setObjectName(QString::fromUtf8("label_39"));
        label_39->setGeometry(QRect(0, 40, 51, 46));
        label_39->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/button_settlement.png) 0 0 0 0 stretch stretch;"));
        verticalLayoutWidget = new QWidget(page_5);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(650, 0, 151, 91));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        label_40 = new QLabel(verticalLayoutWidget);
        label_40->setObjectName(QString::fromUtf8("label_40"));
        label_40->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--grain.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_22->addWidget(label_40);

        label_44 = new QLabel(verticalLayoutWidget);
        label_44->setObjectName(QString::fromUtf8("label_44"));
        label_44->setFont(font4);

        horizontalLayout_22->addWidget(label_44);

        label_41 = new QLabel(verticalLayoutWidget);
        label_41->setObjectName(QString::fromUtf8("label_41"));
        label_41->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--wool.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_22->addWidget(label_41);

        label_45 = new QLabel(verticalLayoutWidget);
        label_45->setObjectName(QString::fromUtf8("label_45"));
        label_45->setFont(font4);

        horizontalLayout_22->addWidget(label_45);


        verticalLayout->addLayout(horizontalLayout_22);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        label_43 = new QLabel(verticalLayoutWidget);
        label_43->setObjectName(QString::fromUtf8("label_43"));
        label_43->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--lumber.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_23->addWidget(label_43);

        label_46 = new QLabel(verticalLayoutWidget);
        label_46->setObjectName(QString::fromUtf8("label_46"));
        label_46->setFont(font4);

        horizontalLayout_23->addWidget(label_46);

        label_42 = new QLabel(verticalLayoutWidget);
        label_42->setObjectName(QString::fromUtf8("label_42"));
        label_42->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--brick.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_23->addWidget(label_42);

        label_47 = new QLabel(verticalLayoutWidget);
        label_47->setObjectName(QString::fromUtf8("label_47"));
        label_47->setFont(font4);

        horizontalLayout_23->addWidget(label_47);


        verticalLayout->addLayout(horizontalLayout_23);

        horizontalLayoutWidget_14 = new QWidget(page_5);
        horizontalLayoutWidget_14->setObjectName(QString::fromUtf8("horizontalLayoutWidget_14"));
        horizontalLayoutWidget_14->setGeometry(QRect(50, 0, 149, 42));
        horizontalLayout_25 = new QHBoxLayout(horizontalLayoutWidget_14);
        horizontalLayout_25->setObjectName(QString::fromUtf8("horizontalLayout_25"));
        horizontalLayout_25->setContentsMargins(0, 0, 0, 0);
        label_52 = new QLabel(horizontalLayoutWidget_14);
        label_52->setObjectName(QString::fromUtf8("label_52"));
        label_52->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--lumber.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_25->addWidget(label_52);

        label_53 = new QLabel(horizontalLayoutWidget_14);
        label_53->setObjectName(QString::fromUtf8("label_53"));
        label_53->setFont(font4);

        horizontalLayout_25->addWidget(label_53);

        label_54 = new QLabel(horizontalLayoutWidget_14);
        label_54->setObjectName(QString::fromUtf8("label_54"));
        label_54->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--brick.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_25->addWidget(label_54);

        label_55 = new QLabel(horizontalLayoutWidget_14);
        label_55->setObjectName(QString::fromUtf8("label_55"));
        label_55->setFont(font4);

        horizontalLayout_25->addWidget(label_55);

        horizontalLayoutWidget_15 = new QWidget(page_5);
        horizontalLayoutWidget_15->setObjectName(QString::fromUtf8("horizontalLayoutWidget_15"));
        horizontalLayoutWidget_15->setGeometry(QRect(50, 40, 149, 41));
        horizontalLayout_26 = new QHBoxLayout(horizontalLayoutWidget_15);
        horizontalLayout_26->setObjectName(QString::fromUtf8("horizontalLayout_26"));
        horizontalLayout_26->setContentsMargins(0, 0, 0, 0);
        label_56 = new QLabel(horizontalLayoutWidget_15);
        label_56->setObjectName(QString::fromUtf8("label_56"));
        label_56->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--grain.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_26->addWidget(label_56);

        label_57 = new QLabel(horizontalLayoutWidget_15);
        label_57->setObjectName(QString::fromUtf8("label_57"));
        label_57->setFont(font4);

        horizontalLayout_26->addWidget(label_57);

        label_58 = new QLabel(horizontalLayoutWidget_15);
        label_58->setObjectName(QString::fromUtf8("label_58"));
        label_58->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--ore.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_26->addWidget(label_58);

        label_59 = new QLabel(horizontalLayoutWidget_15);
        label_59->setObjectName(QString::fromUtf8("label_59"));
        label_59->setFont(font4);

        horizontalLayout_26->addWidget(label_59);

        label_48 = new QLabel(page_5);
        label_48->setObjectName(QString::fromUtf8("label_48"));
        label_48->setGeometry(QRect(0, 560, 51, 46));
        label_48->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/button_magicCard.png) 0 0 0 0 stretch stretch;"));
        layoutWidget = new QWidget(page_5);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(50, 560, 151, 41));
        horizontalLayout_24 = new QHBoxLayout(layoutWidget);
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        horizontalLayout_24->setContentsMargins(0, 0, 0, 0);
        label_49 = new QLabel(layoutWidget);
        label_49->setObjectName(QString::fromUtf8("label_49"));
        label_49->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--grain.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_24->addWidget(label_49);

        label_50 = new QLabel(layoutWidget);
        label_50->setObjectName(QString::fromUtf8("label_50"));
        label_50->setFont(font4);

        horizontalLayout_24->addWidget(label_50);

        label_51 = new QLabel(layoutWidget);
        label_51->setObjectName(QString::fromUtf8("label_51"));
        label_51->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--wool.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_24->addWidget(label_51);

        label_60 = new QLabel(layoutWidget);
        label_60->setObjectName(QString::fromUtf8("label_60"));
        label_60->setFont(font4);

        horizontalLayout_24->addWidget(label_60);

        layoutWidget_2 = new QWidget(page_5);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(50, 600, 71, 41));
        horizontalLayout_27 = new QHBoxLayout(layoutWidget_2);
        horizontalLayout_27->setObjectName(QString::fromUtf8("horizontalLayout_27"));
        horizontalLayout_27->setContentsMargins(0, 0, 0, 0);
        label_61 = new QLabel(layoutWidget_2);
        label_61->setObjectName(QString::fromUtf8("label_61"));
        label_61->setStyleSheet(QString::fromUtf8("border-image: url(:/resources/images/resources--ore.png) 0 0 0 0 stretch stretch;"));

        horizontalLayout_27->addWidget(label_61);

        label_62 = new QLabel(layoutWidget_2);
        label_62->setObjectName(QString::fromUtf8("label_62"));
        label_62->setFont(font4);

        horizontalLayout_27->addWidget(label_62);

        stackedWidget->addWidget(page_5);
        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Catan", nullptr));
        pbExit->setText(QApplication::translate("MainWindow", "EXIT", nullptr));
        pbHelp->setText(QApplication::translate("MainWindow", "?", nullptr));
        pbStartGame->setText(QApplication::translate("MainWindow", "START GAME", nullptr));
        pbSettings->setText(QApplication::translate("MainWindow", "SETTINGS", nullptr));
        label->setText(QApplication::translate("MainWindow", "MUSIC", nullptr));
        pbBackToMenu->setText(QApplication::translate("MainWindow", "BACK TO MAIN MENU", nullptr));
        gbox1->setTitle(QString());
        rbON->setText(QApplication::translate("MainWindow", "ON", nullptr));
        rbOFF->setText(QApplication::translate("MainWindow", "OFF", nullptr));
        gbox2->setTitle(QString());
        pbBackToMenu_2->setText(QApplication::translate("MainWindow", "BACK TO MAIN MENU", nullptr));
        lbPlayer3->setText(QApplication::translate("MainWindow", "P3", nullptr));
        lbPlayer2->setText(QApplication::translate("MainWindow", "P2", nullptr));
        pbBackToMenu_3->setText(QApplication::translate("MainWindow", "BACK", nullptr));
        lbPlayer4->setText(QApplication::translate("MainWindow", "P4", nullptr));
        lbPlayer1->setText(QApplication::translate("MainWindow", "P1", nullptr));
        pbContinue->setText(QApplication::translate("MainWindow", "CONTINUE", nullptr));
        lbPlayer2Required->setText(QString());
        lbPlayer3Required->setText(QString());
        lbPlayer4Required->setText(QString());
        lbPlayer1Required->setText(QString());
        groupBox->setTitle(QString());
        lbPlayer1Name->setText(QApplication::translate("MainWindow", "Player 1", nullptr));
        label_30->setText(QApplication::translate("MainWindow", "Points:", nullptr));
        lbPointsPlayer1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "Roads:", nullptr));
        lbRoadsPlayer1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_10->setText(QString());
        lbWheatP1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_6->setText(QString());
        lbWoolP1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_8->setText(QString());
        lbStoneP1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_7->setText(QString());
        lbWoodP1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_3->setText(QString());
        lbBrickP1->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox_2->setTitle(QString());
        lbPlayer2Name->setText(QApplication::translate("MainWindow", "Player 2", nullptr));
        label_31->setText(QApplication::translate("MainWindow", "Points:", nullptr));
        lbPointsPlayer2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Roads:", nullptr));
        lbRoadsPlayer2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_11->setText(QString());
        lbWheatP2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_13->setText(QString());
        lbWoolP2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_15->setText(QString());
        lbStoneP2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_17->setText(QString());
        lbWoodP2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_18->setText(QString());
        lbBrickP2->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox_3->setTitle(QString());
        lbPlayer3Name->setText(QApplication::translate("MainWindow", "Player 3", nullptr));
        label_33->setText(QApplication::translate("MainWindow", "Points:", nullptr));
        lbPointsPlayer3->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "Roads:", nullptr));
        lbRoadsPlayer3->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_19->setText(QString());
        lbWheatP3->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_20->setText(QString());
        lbWoolP3->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_21->setText(QString());
        lbStoneP3->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_22->setText(QString());
        lbWoodP3->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_23->setText(QString());
        lbBrickP3->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox_4->setTitle(QString());
        lbPlayer4Name->setText(QApplication::translate("MainWindow", "Player 4", nullptr));
        label_35->setText(QApplication::translate("MainWindow", "Points:", nullptr));
        lbPointsPlayer4->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "Roads:", nullptr));
        lbRoadsPlayer4->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_24->setText(QString());
        lbWheatP4->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_25->setText(QString());
        lbWoolP4->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_26->setText(QString());
        lbStoneP4->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_27->setText(QString());
        lbWoodP4->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_28->setText(QString());
        lbBrickP4->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox_5->setTitle(QString());
        pushButton->setText(QString());
        label_4->setText(QApplication::translate("MainWindow", "PLAYER TURN", nullptr));
        lbPlayerTurn->setText(QString());
        label_5->setText(QApplication::translate("MainWindow", "3  FOR  1", nullptr));
        pbTrade->setText(QString());
        pb_Road->setText(QString());
        pb_Settlement->setText(QString());
        pb_MagicCard->setText(QString());
        pb_House->setText(QString());
        label_29->setText(QString());
        lbWheatBank->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_32->setText(QString());
        lbWoolBank->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_34->setText(QString());
        lbStoneBank->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_36->setText(QString());
        lbWoodBank->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_37->setText(QString());
        lbBrickBank->setText(QApplication::translate("MainWindow", "0", nullptr));
        lbBank->setText(QApplication::translate("MainWindow", "Bank", nullptr));
        pbRollDice->setText(QApplication::translate("MainWindow", "ROLL DICE", nullptr));
        label_2->setText(QString());
        label_38->setText(QString());
        label_39->setText(QString());
        label_40->setText(QString());
        label_44->setText(QApplication::translate("MainWindow", "1", nullptr));
        label_41->setText(QString());
        label_45->setText(QApplication::translate("MainWindow", "1", nullptr));
        label_43->setText(QString());
        label_46->setText(QApplication::translate("MainWindow", "1", nullptr));
        label_42->setText(QString());
        label_47->setText(QApplication::translate("MainWindow", "1", nullptr));
        label_52->setText(QString());
        label_53->setText(QApplication::translate("MainWindow", "1", nullptr));
        label_54->setText(QString());
        label_55->setText(QApplication::translate("MainWindow", "1", nullptr));
        label_56->setText(QString());
        label_57->setText(QApplication::translate("MainWindow", "2", nullptr));
        label_58->setText(QString());
        label_59->setText(QApplication::translate("MainWindow", "3", nullptr));
        label_48->setText(QString());
        label_49->setText(QString());
        label_50->setText(QApplication::translate("MainWindow", "1", nullptr));
        label_51->setText(QString());
        label_60->setText(QApplication::translate("MainWindow", "1", nullptr));
        label_61->setText(QString());
        label_62->setText(QApplication::translate("MainWindow", "1", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
