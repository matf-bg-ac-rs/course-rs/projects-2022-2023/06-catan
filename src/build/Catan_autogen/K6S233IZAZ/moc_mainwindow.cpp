/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "headers/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[35];
    char stringdata0[586];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 17), // "AddedNewBlankNode"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 9), // "GUI_Node*"
QT_MOC_LITERAL(4, 40, 4), // "node"
QT_MOC_LITERAL(5, 45, 8), // "AddHouse"
QT_MOC_LITERAL(6, 54, 22), // "on_pbStartGame_clicked"
QT_MOC_LITERAL(7, 77, 21), // "on_pbSettings_clicked"
QT_MOC_LITERAL(8, 99, 23), // "on_pbBackToMenu_clicked"
QT_MOC_LITERAL(9, 123, 25), // "on_pbBackToMenu_2_clicked"
QT_MOC_LITERAL(10, 149, 17), // "on_pbHelp_clicked"
QT_MOC_LITERAL(11, 167, 17), // "on_pbExit_clicked"
QT_MOC_LITERAL(12, 185, 21), // "on_pbContinue_clicked"
QT_MOC_LITERAL(13, 207, 25), // "on_pbBackToMenu_3_clicked"
QT_MOC_LITERAL(14, 233, 15), // "on_rbON_toggled"
QT_MOC_LITERAL(15, 249, 7), // "checked"
QT_MOC_LITERAL(16, 257, 16), // "on_rbOFF_toggled"
QT_MOC_LITERAL(17, 274, 19), // "on_pb_House_clicked"
QT_MOC_LITERAL(18, 294, 20), // "manageResourcesHouse"
QT_MOC_LITERAL(19, 315, 10), // "houseBuilt"
QT_MOC_LITERAL(20, 326, 12), // "invalidHouse"
QT_MOC_LITERAL(21, 339, 18), // "on_pb_Road_clicked"
QT_MOC_LITERAL(22, 358, 19), // "manageResourcesRoad"
QT_MOC_LITERAL(23, 378, 9), // "roadBuilt"
QT_MOC_LITERAL(24, 388, 11), // "invalidRoad"
QT_MOC_LITERAL(25, 400, 24), // "on_pb_Settlement_clicked"
QT_MOC_LITERAL(26, 425, 19), // "manageResourcesCity"
QT_MOC_LITERAL(27, 445, 9), // "cityBuilt"
QT_MOC_LITERAL(28, 455, 11), // "invalidCity"
QT_MOC_LITERAL(29, 467, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(30, 489, 21), // "on_pbRollDice_clicked"
QT_MOC_LITERAL(31, 511, 23), // "on_pb_MagicCard_clicked"
QT_MOC_LITERAL(32, 535, 18), // "on_pbTrade_clicked"
QT_MOC_LITERAL(33, 554, 16), // "tradeSuccessfull"
QT_MOC_LITERAL(34, 571, 14) // "magicCardDrawn"

    },
    "MainWindow\0AddedNewBlankNode\0\0GUI_Node*\0"
    "node\0AddHouse\0on_pbStartGame_clicked\0"
    "on_pbSettings_clicked\0on_pbBackToMenu_clicked\0"
    "on_pbBackToMenu_2_clicked\0on_pbHelp_clicked\0"
    "on_pbExit_clicked\0on_pbContinue_clicked\0"
    "on_pbBackToMenu_3_clicked\0on_rbON_toggled\0"
    "checked\0on_rbOFF_toggled\0on_pb_House_clicked\0"
    "manageResourcesHouse\0houseBuilt\0"
    "invalidHouse\0on_pb_Road_clicked\0"
    "manageResourcesRoad\0roadBuilt\0invalidRoad\0"
    "on_pb_Settlement_clicked\0manageResourcesCity\0"
    "cityBuilt\0invalidCity\0on_pushButton_clicked\0"
    "on_pbRollDice_clicked\0on_pb_MagicCard_clicked\0"
    "on_pbTrade_clicked\0tradeSuccessfull\0"
    "magicCardDrawn"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  164,    2, 0x06 /* Public */,
       5,    0,  167,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  168,    2, 0x08 /* Private */,
       7,    0,  169,    2, 0x08 /* Private */,
       8,    0,  170,    2, 0x08 /* Private */,
       9,    0,  171,    2, 0x08 /* Private */,
      10,    0,  172,    2, 0x08 /* Private */,
      11,    0,  173,    2, 0x08 /* Private */,
      12,    0,  174,    2, 0x08 /* Private */,
      13,    0,  175,    2, 0x08 /* Private */,
      14,    1,  176,    2, 0x08 /* Private */,
      16,    1,  179,    2, 0x08 /* Private */,
      17,    0,  182,    2, 0x08 /* Private */,
      18,    0,  183,    2, 0x08 /* Private */,
      19,    0,  184,    2, 0x08 /* Private */,
      20,    0,  185,    2, 0x08 /* Private */,
      21,    0,  186,    2, 0x08 /* Private */,
      22,    0,  187,    2, 0x08 /* Private */,
      23,    0,  188,    2, 0x08 /* Private */,
      24,    0,  189,    2, 0x08 /* Private */,
      25,    0,  190,    2, 0x08 /* Private */,
      26,    0,  191,    2, 0x08 /* Private */,
      27,    0,  192,    2, 0x08 /* Private */,
      28,    0,  193,    2, 0x08 /* Private */,
      29,    0,  194,    2, 0x08 /* Private */,
      30,    0,  195,    2, 0x08 /* Private */,
      31,    0,  196,    2, 0x08 /* Private */,
      32,    0,  197,    2, 0x08 /* Private */,
      33,    0,  198,    2, 0x08 /* Private */,
      34,    0,  199,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   15,
    QMetaType::Void, QMetaType::Bool,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->AddedNewBlankNode((*reinterpret_cast< GUI_Node*(*)>(_a[1]))); break;
        case 1: _t->AddHouse(); break;
        case 2: _t->on_pbStartGame_clicked(); break;
        case 3: _t->on_pbSettings_clicked(); break;
        case 4: _t->on_pbBackToMenu_clicked(); break;
        case 5: _t->on_pbBackToMenu_2_clicked(); break;
        case 6: _t->on_pbHelp_clicked(); break;
        case 7: _t->on_pbExit_clicked(); break;
        case 8: _t->on_pbContinue_clicked(); break;
        case 9: _t->on_pbBackToMenu_3_clicked(); break;
        case 10: _t->on_rbON_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->on_rbOFF_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->on_pb_House_clicked(); break;
        case 13: _t->manageResourcesHouse(); break;
        case 14: _t->houseBuilt(); break;
        case 15: _t->invalidHouse(); break;
        case 16: _t->on_pb_Road_clicked(); break;
        case 17: _t->manageResourcesRoad(); break;
        case 18: _t->roadBuilt(); break;
        case 19: _t->invalidRoad(); break;
        case 20: _t->on_pb_Settlement_clicked(); break;
        case 21: _t->manageResourcesCity(); break;
        case 22: _t->cityBuilt(); break;
        case 23: _t->invalidCity(); break;
        case 24: _t->on_pushButton_clicked(); break;
        case 25: _t->on_pbRollDice_clicked(); break;
        case 26: _t->on_pb_MagicCard_clicked(); break;
        case 27: _t->on_pbTrade_clicked(); break;
        case 28: _t->tradeSuccessfull(); break;
        case 29: _t->magicCardDrawn(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< GUI_Node* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MainWindow::*)(GUI_Node * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::AddedNewBlankNode)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (MainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::AddHouse)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::AddedNewBlankNode(GUI_Node * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::AddHouse()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
