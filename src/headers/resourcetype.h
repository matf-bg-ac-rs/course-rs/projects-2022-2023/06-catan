#ifndef RESOURCETYPE_H
#define RESOURCETYPE_H


enum class ResourceType {
    Wood, //drvo
    Brick,   //cigla
    Wool,  //vuna
    Wheat,  //psenica
    Stone   //kamen
};

#endif // RESOURCETYPE_H
