## Catan  <br><br>

## :memo: Opis igre
- Strateska drustvena igra u kojoj se 4 igraca medjusobno takmice tako sto dobijaju resurse u zavisnosti od bacanja kockica, koje kasnije koriste u izgradnji puteva, naselja i gradova. Pobednik je onaj ko prvi stigne do 10 poena. <br><br>

## :movie_camera: Demo snimak projekta 
- link: [Catan](https://www.youtube.com/watch?v=ufzqkQhe1UI) <br><br>

## :black_circle: Okruzenje
- [![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-olivia)](https://www.qt.io/download) <br><br>


## :white_circle: Programski jezik
- [![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-red)](https://www.cplusplus.com/)  *C++17*  <br>
- [![qt5](https://img.shields.io/badge/Framework-Qt5-blue)](https://doc.qt.io/qt-6/)  <br><br>


## :hammer: Instalacija :
- Preuzeti i instalirati [*Qt* i *Qt Creator*](https://www.qt.io/download).
- Ako je to potrebno,  nadograditi verziju C++ na C++17 
<br><br>

## :notebook: Biblioteke :
- QtMultimedia


## :wrench: Preuzimanje i pokretanje :
- 1. U terminalu se pozicionirati u zeljeni direktorijum
- 2. Klonirati repozitorijum komandom: `$ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/06-catan.git`
- 3. Otvoriti okruzenje *Qt Creator* i u njemu otvoriti Catan.pro fajl
- 4. Pritisnuti dugme Run u donjem levom uglu ekrana
<br><br>


## Developers

<ul>
    <li><a href="https://gitlab.com/LukaBura">Luka Bura 218/2019</a></li>
    <li><a href="https://gitlab.com/luka_120_2019">Luka Vukotić 120/2019</a></li>
    <li><a href="https://gitlab.com/stefan_103_2019">Stefan Drljević 103/2019</a></li>
    <li><a href="https://gitlab.com/snele45">Stefan Nešković 386/2021</a></li>
</ul>
